import time
import board
import pwmio
from digitalio import DigitalInOut, Direction
from adafruit_motor import servo
pwm = pwmio.PWMOut(board.A3, duty_cycle=2**15, frequency=50)
my_servo = servo.Servo(pwm)
hdd_sense = DigitalInOut(board.D1)
hdd_on = DigitalInOut(board.D2)
hdd_sense.direction = Direction.INPUT
hdd_on.direction = Direction.INPUT

my_servo.angle = 0
angle = 1 
while True:
    if not hdd_sense.value: 
        print("HDD is on")
    else:
        print("HDD is off")
        hdd_on.direction = Direction.OUTPUT
        hdd_on.value = 1
        time.sleep(0.2)
        hdd_on.value = 0
        hdd_on.direction = Direction.INPUT
    time.sleep(1)
    # angle += 1
    # if angle > 180:
    #     angle = 0
